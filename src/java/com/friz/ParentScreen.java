/**
 * Copyright (c) 2015 Kyle Friz
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.friz;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.beans.property.DoubleProperty;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import javafx.util.Duration;

/**
 * @author Kyle Friz
 * @since  Nov 1, 2015
 */
public class ParentScreen extends StackPane {

	private final Map<String, Link> screens = new HashMap<>();
	private final Stage stage;

	public ParentScreen(Stage stage) {
		this.stage = stage;
	}

	public void initialize(String name, String fxml) throws IOException {
		final FXMLLoader loader = new FXMLLoader(getClass().getResource(fxml));
		
		Parent parent = (Parent) loader.load();
		
		Controller controller = (Controller) loader.getController();
		controller.setParent(this);
		
		screens.put(name, new Link(parent, controller));
	}
	
	public void destroy(String name) {
		screens.remove(name);
	}
	
	public void display(String name) {
		Link link = screens.get(name);
		if (link != null) {
			DoubleProperty opacity = opacityProperty();
			
			if (!getChildren().isEmpty()) {
				Timeline fade = new Timeline(
						new KeyFrame(Duration.ZERO, new KeyValue(opacity, 1.0)),
						new KeyFrame(new Duration(1000), new EventHandler<ActionEvent>() {
							@Override
							public void handle(ActionEvent t) {
								getChildren().remove(0);
								getChildren().add(0, link.getParent());
								
								Timeline fadeIn = new Timeline(
										new KeyFrame(Duration.ZERO, new KeyValue(opacity, 0.0)),
										new KeyFrame(new Duration(800), new EventHandler<ActionEvent>() {

											@Override
											public void handle(ActionEvent event) {
												link.getController().start();
												stage.sizeToScene();
												stage.centerOnScreen();
											}
											
										}, new KeyValue(opacity, 1.0)));
								
								fadeIn.play();
							}
						}, new KeyValue(opacity, 0.0)));
				fade.play();
			} else {
				setOpacity(0.0);
				getChildren().add(link.getParent());
				
				Timeline fadeIn = new Timeline(
						new KeyFrame(Duration.ZERO, new KeyValue(opacity, 0.0)),
						new KeyFrame(new Duration(2500), new EventHandler<ActionEvent>() {

							@Override
							public void handle(ActionEvent event) {
								link.getController().start();
								stage.sizeToScene();
								stage.centerOnScreen();
							}
							
						}, new KeyValue(opacity, 1.0)));
				
				fadeIn.play();
			}
		}
	}
	
	public final Stage stage() {
		return stage;
	}
	
}
