/**
 * Copyright (c) 2015 Kyle Friz
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.friz.frame;

import java.awt.Color;
import java.net.URL;
import java.util.ResourceBundle;

import javax.swing.SwingUtilities;

import com.friz.Controller;
import com.friz.ParentScreen;

import javafx.application.Platform;
import javafx.embed.swing.SwingNode;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;

/**
 * @author Kyle Friz
 * @since  Nov 1, 2015
 */
public class FrameController implements Initializable, Controller {

	@FXML
	private SwingNode node;
	
	private GameApplet stub;
	private ParentScreen parent;
	
	/* (non-Javadoc)
	 * @see javafx.fxml.Initializable#initialize(java.net.URL, java.util.ResourceBundle)
	 */
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		//stub = new GameApplet();
		//stub.initialize();
	}
	
	/* (non-Javadoc)
	 * @see com.friz.Controler#setParent(com.friz.ParentScreen)
	 */
	@Override
	public void setParent(ParentScreen parent) {
		this.parent = parent;
	}

	/* (non-Javadoc)
	 * @see com.friz.Controller#start()
	 */
	@Override
	public void start() {
		stub = new GameApplet();
		//stub.getContentPane().setBackground(Color.BLACK);
		node.setContent(stub.getRootPane());
		SwingUtilities.invokeLater(() -> {
			//stub.start();
			Platform.runLater(() -> {
				//JRootPane p = new JRootPane();
				//p.setSize(new Dimension(783, 545));
				//p.add(stub.getApplet());
				//node.setContent(stub.getRootPane());
				//stub.setSize(783, 545);
				//stub.start();
				//stub = new GameApplet();
			});
		});
	}
	
	/* (non-Javadoc)
	 * @see com.friz.Controler#destoy()
	 */
	@Override
	public void destoy() {
		parent.destroy("frame");
	}

}
