package com.friz.frame;

import java.applet.Applet;

/**
 * Copyright (c) 2015 Kyle Friz
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import java.applet.AppletContext;
import java.applet.AppletStub;
import java.awt.Color;
import java.awt.Dimension;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import javax.swing.JButton;
import javax.swing.JFrame;

/**
 * @author Kyle Friz
 * @date Apr 9, 2015
 */
public class GameApplet extends JFrame implements AppletStub {

	private static final long serialVersionUID = -1657955994522660237L;

	private String host;
	private Map<String, String> map;
	
	Class<?> client;
	Applet v_client;

	public GameApplet() {
		//super();
		System.out.println("test");
		setBackground(Color.BLACK);
		getContentPane().setPreferredSize(new Dimension(764, 545));
		//setSize(763, 545);
		//setSize(783, 545);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		getContentPane().setVisible(true);
		//setVisible(true);
		initialize();
	}

	public void initialize() {
		try {
			client = Class.forName("Client");
			map = new HashMap<>();
			host = new String("http://oldschool6.runescape.com");

			map.put("11", "false");
			map.put("12", "0");
			map.put("13", "0");
			map.put("14", "true");
			map.put("15", ".runescape.com");;
			map.put("1", "0");
			map.put("2", "ElZAIrq5NpKN6D3mDdihco3oPeYN2KFy2DCquj7JMmECPmLrDP3Bnw");
			map.put("3", "1");
			map.put("4", "2217");
			map.put("5", "306");
			map.put("6", "true");
			map.put("7", "http://www.runescape.com/g=oldscape/slr.ws?order=LPWM");
			map.put("8", "5");
			map.put("9", "0");
			map.put("10", "");


			v_client = (Applet) client.getConstructor().newInstance();
			client.getSuperclass().getMethod("setStub", AppletStub.class)
					.invoke(v_client, this);
			//v_client.init();
			//v_client.start();
			v_client.setPreferredSize(new Dimension(763, 545));
			//setPreferredSize(new Dimension(763, 545));
			//getContentPane().add(v_client, "Center");
			getContentPane().add(v_client, "Center");
			start();
			//setVisible(true);
			pack();
			//start();
			//repaint();
			//setVisible(true);

			System.out.println(v_client.isValid());
			System.out.println(v_client.isDisplayable());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void start() {
		try {
			client.getMethod("init").invoke(v_client);
			client.getMethod("start").invoke(v_client);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public Applet getApplet() { return v_client; }
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see java.applet.AppletStub#appletResize(int, int)
	 */
	@Override
	public void appletResize(int width, int height) {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.applet.AppletStub#getAppletContext()
	 */
	@Override
	public AppletContext getAppletContext() {
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.applet.AppletStub#getCodeBase()
	 */
	@Override
	public URL getCodeBase() {
		try {
			return new URL(host);
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.applet.AppletStub#getDocumentBase()
	 */
	@Override
	public URL getDocumentBase() {
		try {
			return new URL(host);
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.applet.AppletStub#getParameter(java.lang.String)
	 */
	@Override
	public String getParameter(String key) {
		return map.get(key);
	}
}
