/**
 * Copyright (c) 2015 Kyle Friz
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.friz.login;

import java.awt.Desktop;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ResourceBundle;

import com.friz.Controller;
import com.friz.ParentScreen;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.CheckBox;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;

/**
 * @author Kyle Friz
 * @since  Nov 1, 2015
 */
public class LoginController implements Initializable, Controller {

	private ParentScreen parent;
	
	@FXML
	private TextField username;

	@FXML
	private PasswordField password;

	@FXML
	private CheckBox remember;
	
	/* (non-Javadoc)
	 * @see javafx.fxml.Initializable#initialize(java.net.URL, java.util.ResourceBundle)
	 */
	@Override
	public void initialize(URL location, ResourceBundle resources) { }

	/* (non-Javadoc)
	 * @see com.friz.Controler#setParent(com.friz.ParentScreen)
	 */
	@Override
	public void setParent(ParentScreen parent) {
		this.parent = parent;
	}
	
	/* (non-Javadoc)
	 * @see com.friz.Controller#start()
	 */
	@Override
	public void start() { }
	
	/* (non-Javadoc)
	 * @see com.friz.Controler#destoy()
	 */
	@Override
	public void destoy() { 
		parent.destroy("login");
	}
	
	@FXML
	private void visit(ActionEvent a) {
		if (Desktop.isDesktopSupported()) {
			try {
				Desktop.getDesktop().browse(new URI("http://www.friz.net/"));
			} catch (IOException | URISyntaxException e) {
				e.printStackTrace();
			}
		}
	}
	
	@FXML
	private void login(ActionEvent a) {
		System.out.println(username.getText() + ", " + password.getText() + ", " + remember.isSelected());
		parent.display("frame");
	}

}
